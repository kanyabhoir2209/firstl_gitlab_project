import React, { useState, useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { Button, IconButton, Typography, Grid, LinearProgress } from '@material-ui/core';
import { Delete as DeleteIcon, CloudUpload as CloudUploadIcon } from '@material-ui/icons';
import PreviewAsset from './PreviewAsset';
import { FileResult } from '../types';
import shortid from 'shortid';
import {UPLOAD_ASSET_TYPE} from '../types';



interface UploadAssetProps {
	maxVideoTime: number;
	reset: boolean;
	disabled: boolean;
	source: FileResult;
	uploadAssetType: UPLOAD_ASSET_TYPE;
	handleDelete: () => void;
	handleError: (error: string) => void;
	handleFileSelected: (fileName: string) => void;
    handleUploadComplete: (result: FileResult) => void;
    uploadAssetFn: any;
}

/**
 * Component rendered for Previewing Video or images using srcpath taken as prop.
 * Result = {fileName, duration, assetId, url, uploadstatus}
 */
export default function UploadAsset(props: UploadAssetProps) {
	const {
		maxVideoTime,
		reset,
		disabled,
		source,
		uploadAssetType,
		handleDelete,
		handleError,
		handleFileSelected,
        handleUploadComplete,
        uploadAssetFn
	} = props;

	const fileid: string = shortid.generate();
	const classes = useStyles();
	const [uploadProgress, setUploadProgress] = useState(0);
	const [progressbarVisibility, setProgressbarVisibility] = useState(false);
	const [result, setResult] = useState<FileResult>({
		fileName: '',
		duration: 0,
		id: 0,
		url: '',
		uploadStatus: '',
		mediaType: '',
	});
	const [error, setError] = useState(null);
	const videoAcceptString = "video/mp4,video/x-m4v,video/*";
	const imageAcceptString = "image/*";
	useEffect(() => {
		if (reset) {
			resetState();
		}
	}, [reset]);

	useEffect(() => {
		setResult(source);
	}, [source]);

	const upload = async (event: any) => {
		const file = event.target.files[0];

		validateAsset(file)
			.then(async (resultObj: FileResult) => {
				setProgressbarVisibility(true);
				resultObj.duration = Math.floor(resultObj.duration);
				resultObj.fileName = file.name;

				handleFileSelected(file.name);

				if (
					file !== undefined &&
					file !== null &&
					file.size !== undefined &&
					file.size !== null &&
					file.size
				) {
					const formData = new FormData();
					formData.append('file', file);
					let progressCompleted;
					const config = {
						onUploadProgress: function (progressEvent: any) {
							progressCompleted = Math.round(
								(progressEvent.loaded * 100) / progressEvent.total,
							);
							setUploadProgress(progressCompleted);
							if (progressCompleted > 99) {
								setProgressbarVisibility(false);
							}
						},
					};
					try{
						const resp:any = await uploadAssetFn(formData,config,uploadAssetType);
						resultObj.id = resp.id;
						resultObj.url = resp.url;
						resultObj.uploadStatus = 'Uploaded successfully';
						setResult(resultObj);
						handleUploadComplete(resultObj);
					}
					catch(error) {
						setError(error);
						handleError(error);
					}
				}
			})
			.catch((error) => {
				setError(error);
				handleError(error);
			});
	};

	const validateAsset = async (file: any): Promise<any> => {
		return new Promise((resolve, reject) => {
			const fileType = typeOfFile(file);

			if (fileType === 'image') {
				const resultObj = { ...result };
				resultObj.duration = 0;
				resultObj.mediaType = fileType;
				setResult(resultObj);
				resolve(resultObj);
			}

			const video = document.createElement('video');
			video.preload = 'metadata';
			video.src = window.URL.createObjectURL(file);

			video.onloadedmetadata = function () {
				window.URL.revokeObjectURL(video.src);
				let errorMessage: any;
				if (video.duration < 1) {
					errorMessage = 'Invalid Video! video is less than 1 second';
					setError(errorMessage);
					reject(errorMessage);
				} else if (video.duration > maxVideoTime) {
					errorMessage = 'Video size exceeded';
					setError(errorMessage);
					reject(errorMessage);
				} else {
					const resultObj = { ...result };
					resultObj.duration = video.duration;
					resultObj.mediaType = 'video';
					setResult(resultObj);
					resolve(resultObj);
				}
			};
		});
	};

	const typeOfFile = (file: any) => {
		return file && file['type'].split('/')[0];
	};

	const resetState = () => {
		setProgressbarVisibility(false);
		setUploadProgress(0);
		const resultObj: FileResult = {
			fileName: '',
			duration: 0,
			id: 0,
			url: '',
			uploadStatus: '',
			mediaType: '',
		};
		setResult(resultObj);
		setError(null);
	};

	return (
		<>
			{progressbarVisibility ? (
				<>
					<LinearProgress variant="determinate" value={uploadProgress} />

					<Typography>
						{uploadProgress && uploadProgress.toString()}%
					</Typography>
				</>
			) : (
				<></>
			)}

			{result && result.url && result.url.length > 0 ? (
				<Grid
					container
					direction="row"
					alignItems="center"
					justify="flex-start"
					className={classes.uploadParent}>
					<Grid xs={3} item>
						<PreviewAsset
							srcpath={result.url}
							type={result.mediaType}
							filename={result.fileName}
							buttontype="thumbnail"
						/>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="subtitle2">
							{result.fileName}
						</Typography>
					</Grid>

					<Grid item xs={3}>
						<IconButton
							aria-label="delete"
							onClick={() => handleDelete()}
							disabled={disabled}>
							<DeleteIcon fontSize="small" />
						</IconButton>
					</Grid>
				</Grid>
			) : (
				<>
					<input
						className={classes.uploadInput}
						id={fileid}
						onChange={upload}
						type="file"
						accept={ result.mediaType === "video" ? videoAcceptString : imageAcceptString}
					/>
					<label htmlFor={fileid}>
						<Button
							variant="contained"
							startIcon={<CloudUploadIcon />}
							color="primary"
							component="span"
							disabled={disabled}>
							Upload
						</Button>
					</label>
				</>
			)}
		</>
	);
}

const useStyles = makeStyles((theme) =>
	createStyles({
		uploadInput: {
			display: 'none',
		},
		errorMessage: {
			color: 'red',
		},
		uploadParent: {
			border: '1px solid #085DAD',
			padding: theme.spacing(1),
		}
	}),
);
